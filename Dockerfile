FROM     tomcat:9.0.50-jdk11-adoptopenjdk-hotspot
LABEL    description="Apache Tomcat for Local environment" 

EXPOSE   "8080"

CMD      ["sh", "-c", "catalina.sh run"]
